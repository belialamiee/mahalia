local Mahalia = {}

--initial set up of variables so that we can initialise the ui
local curXP = GetUnitXP("Player")
local maxXP = GetUnitXPMax("Player")
local lvl = GetUnitLevel("Player")




--FUNCTIONS

-- called on initialise to run the UI display
function Mahalia:OnUpdate()
	combatIndicator:SetHidden(true)
	curXP = GetUnitXP("Player")
	maxXP = GetUnitXPMax("Player")
	local lvl = GetUnitLevel("Player")
	MahaliaXpgain:SetText(string.format("Lvl: %d XP: %d/%d", lvl, curXP, maxXP))
end

--on xpupdate, update the ui display
function Mahalia:OnXpUpdate(eventCode, unitTag, currentExp, maxExp, reason )
	--get the data to display to the display
	curXP = GetUnitXP("Player")
	maxXP = GetUnitXPMax("Player")
	lvl = GetUnitLevel("Player")
	--update the display
	MahaliaXpgain:SetText(string.format("Lvl: %d XP: %d/%d", lvl, curXP, maxXP))
end

--in combat display
function Mahalia:OnCombatMode(eventCode, inCombat)
	if (inCombat) then 
		combatIndicator:SetHidden(false)
	else 
		combatIndicator:SetHidden(true) 
 	end
end



--EVENT REGISTRATION
--register all xp gains

EVENT_MANAGER:RegisterForEvent("MahaliaXPUpdate", EVENT_EXPERIENCE_UPDATE, function(...) Mahalia:OnXpUpdate(...) end)



--register for in and out of combat
EVENT_MANAGER:RegisterForEvent("MahaliaCombatState", EVENT_PLAYER_COMBAT_STATE, function(...) Mahalia:OnCombatMode(...) end)

--register when the player is loaded
EVENT_MANAGER:RegisterForEvent("MahaliaPlayerReady", EVENT_PLAYER_ACTIVATED, function(...) Mahalia:OnUpdate(...) end)



